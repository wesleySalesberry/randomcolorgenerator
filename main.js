const btn = document.querySelector('.colorBTN');
body = document.querySelector('#body');

btn.addEventListener('click', () => {
	document.body.style.background = randomColor();
});

function randomColor() {
	const colors = '123456789ABCDEF';
	let color = '#';

	for (let x = 0; x < 6; x++) {
		color += colors[Math.floor(Math.random() * 16)];
	}

	return color;
}
